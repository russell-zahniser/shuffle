self.addEventListener("install", (event) => {
  event.waitUntil(caches.open("simple-sw-v1")
        .then((cache) => cache.addAll(["index.html", "main.js", "main.css" ])));
});

self.addEventListener("fetch", (event) => {
  event.respondWith(caches.match(event.request)
        .then((response) => response || fetch(event.request)));
});
