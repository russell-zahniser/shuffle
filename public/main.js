// https://www.sitepoint.com/jquery-document-ready-plain-javascript/
function onReady(callback) {
    if (document.readyState === "complete" ||
            (document.readyState !== "loading" && !document.documentElement.doScroll)) {
        callback();
    } else {
        document.addEventListener("DOMContentLoaded", callback);
    }
}

onReady(() => {
    document.body.innerHTML = "Ready.";
});
